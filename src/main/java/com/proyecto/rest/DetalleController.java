package com.proyecto.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.models.traspaso_detalle;
import com.proyecto.service.DetalleService;;

@RestController
@RequestMapping(path  = "/detalle/")
public class DetalleController {
	@Autowired
	private DetalleService detSer;
	@PostMapping(path="/guardar")
	public traspaso_detalle registrarDetalle(@Valid @RequestBody traspaso_detalle det){
		return detSer.crearEdificio(det);
	}
	@GetMapping(path = "/listar")
	public List<traspaso_detalle> listar(){
		return detSer.listar();
	}
	
	@GetMapping(path = "/listar/{nombre}")
	public List<traspaso_detalle> buscarpornombre(@PathVariable String cedula) {
		return detSer.buscarporcedula(cedula);
	}
	
	@DeleteMapping("eliminar/{id}")
	public ResponseEntity<?>delete(@PathVariable int id){
		traspaso_detalle det= detSer.buscarporid(id);
		if(det==null){
			ResponseEntity.badRequest().build();
		}
		detSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<traspaso_detalle> updateEdificio(@PathVariable(value = "id") int id, @RequestBody traspaso_detalle det){
		traspaso_detalle actualizar = detSer.buscarporid(id);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setTdet_cedula(det.getTdet_cedula());
		actualizar.setTdet_cab_id(det.getTdet_cab_id());
		actualizar.setTdet_inv_id(det.getTdet_inv_id());
		return ResponseEntity.ok(detSer.crearEdificio(actualizar));
	}
}
