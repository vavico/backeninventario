package com.proyecto.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.models.aula;
import com.proyecto.service.AulaService;

@RestController
@RequestMapping(path  = "/aula/")
public class RestAulaController {
	
	@Autowired
	private AulaService aulaSer;
	@PostMapping(path="/guardar")
	public aula registrarAula(@Valid @RequestBody aula aula){
		return aulaSer.crearAula(aula);
	}
	@GetMapping(path = "/listar")
	public List<aula> listar(){
		return aulaSer.listar();
	}
	
	@GetMapping(path = "/listar/{nombre}")
	public aula buscarpornombre(@PathVariable String nombre) {
		return aulaSer.buscarpornombre(nombre);
	}
	
	@DeleteMapping("eliminar/{id}")
	public ResponseEntity<?>delete(@PathVariable int id){
		aula aula= aulaSer.buscarporid(id);
		if(aula==null){
			ResponseEntity.badRequest().build();
		}
		aulaSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<aula> updateAula(@PathVariable(value = "id") int id, @RequestBody aula aul){
		aula actualizar = aulaSer.buscarporid(id);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setAula_nombre(aul.getAula_nombre());
		actualizar.setAula_edif_id(aul.getAula_edif_id());
		return ResponseEntity.ok(aulaSer.crearAula(actualizar));
	}
}
