package com.proyecto.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.models.edificio;
import com.proyecto.service.EdificioService;

@RestController
@RequestMapping(path  = "/edificio/")
public class RestEdificioController {
	@Autowired
	private EdificioService edifSer;
	@PostMapping(path="/guardar")
	public edificio registrarEdificio(@Valid @RequestBody edificio edif){
		return edifSer.crearEdificio(edif);
	}
	@GetMapping(path = "/listar")
	public List<edificio> listar(){
		return edifSer.listar();
	}
	
	@GetMapping(path = "/listar/{nombre}")
	public edificio buscarpornombre(@PathVariable String nombre) {
		return edifSer.buscarpornombre(nombre);
	}
	
	@DeleteMapping("eliminar/{id}")
	public ResponseEntity<?>delete(@PathVariable int id){
		edificio edif= edifSer.buscarporid(id);
		if(edif==null){
			ResponseEntity.badRequest().build();
		}
		edifSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<edificio> updateEdificio(@PathVariable(value = "id") int id, @RequestBody edificio edif){
		edificio actualizar = edifSer.buscarporid(id);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setEdif_nombre(edif.getEdif_nombre());
		return ResponseEntity.ok(edifSer.crearEdificio(actualizar));
	}
}
