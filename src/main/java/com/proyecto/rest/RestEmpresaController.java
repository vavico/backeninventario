package com.proyecto.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.models.empresa;
import com.proyecto.service.EmpresaService;

@RestController
@RequestMapping(path  = "/empresa/")
public class RestEmpresaController {
	@Autowired
	private EmpresaService empSer;
	@PostMapping(path="/guardar")
	public empresa registrarEmpresa(@Valid @RequestBody empresa emp){
		return empSer.crearEmpresa(emp);
	}
	@GetMapping(path = "/listar")
	public List<empresa> listar(){
		return empSer.listar();
	}
	
	@GetMapping(path = "/listar/{ruc}")
	public empresa buscarporcedula(@PathVariable String ruc) {
		return empSer.buscarporRuc(ruc);
	}
        
    @GetMapping(path = "/listar/empresa/{nombre}")
	public empresa buscarporNombre(@PathVariable String nombre) {
		return empSer.buscarpornombre(nombre);
	}
	@DeleteMapping("eliminar/{id}")
	public ResponseEntity<?>delete(@PathVariable int id){
		empresa emp = empSer.buscarporid(id);
		if(emp==null){
			ResponseEntity.badRequest().build();
		}
		empSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<empresa> updateEmpresa(@PathVariable(value = "id") int id, @RequestBody empresa emp){
		empresa actualizar = empSer.buscarporid(id);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setEmp_ruc(emp.getEmp_ruc());
		actualizar.setEmp_nombre(emp.getEmp_nombre());
		return ResponseEntity.ok(empSer.crearEmpresa(actualizar));
	}   
}
