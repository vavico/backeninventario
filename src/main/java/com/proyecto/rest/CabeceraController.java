package com.proyecto.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.models.Traspaso_cabecera;
import com.proyecto.service.CabeceraService;

@RestController
@RequestMapping(path  = "/cabecera/")
public class CabeceraController {
	
	@Autowired
	private CabeceraService cabSer;
	@PostMapping(path="/guardar")
	public Traspaso_cabecera registrarCabecera(@Valid @RequestBody Traspaso_cabecera cab){
		return cabSer.crearCabecera(cab);
	}
	@GetMapping(path = "/listar")
	public List<Traspaso_cabecera> listar(){
		return cabSer.listar();
	}
	
	@GetMapping(path = "/listar/{nombre}")
	public List<Traspaso_cabecera> buscarporfecha(@PathVariable String fecha) {
		return cabSer.buscarporfecha(fecha);
	}
	
	@DeleteMapping("eliminar/{id}")
	public ResponseEntity<?>delete(@PathVariable int id){
		Traspaso_cabecera edif= cabSer.buscarporid(id);
		if(edif==null){
			ResponseEntity.badRequest().build();
		}
		cabSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Traspaso_cabecera> updateCabecera(@PathVariable(value = "id") int id, @RequestBody Traspaso_cabecera cab){
		Traspaso_cabecera actualizar = cabSer.buscarporid(id);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setTcab_fecha(cab.getTcab_fecha());
		return ResponseEntity.ok(cabSer.crearCabecera(actualizar));
	}
}
