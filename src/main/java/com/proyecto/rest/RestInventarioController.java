package com.proyecto.rest;

//Clase de los metodos post

import java.util.List;
import com.proyecto.models.inventario;
import com.proyecto.service.InventarioService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path  = "/inventario/")
public class RestInventarioController {
	
	@Autowired
	private InventarioService invSer;
	@PostMapping(path="/guardar")
	public inventario registrarInventario(@Valid @RequestBody inventario inv){
		return invSer.crearInventario(inv);
	}
	@GetMapping(path = "/listar")
	public List<inventario> listar(){
		return invSer.listar();
	}
	
	@GetMapping(path = "/listar/{buscar}")
	public List<inventario> buscar(@PathVariable String buscar) {
		return invSer.buscar(buscar);
	}
        
    @GetMapping(path = "/listar/cedula/{buscar}")
	public List<inventario> buscarCedula(@PathVariable String buscar) {
		return invSer.buscarCedula(buscar);
	}

        @DeleteMapping("eliminar/{id}")
        public ResponseEntity<?>delete(@PathVariable int id){
            inventario inv = invSer.buscarporid(id);
            if(inv==null){
                ResponseEntity.badRequest().build();
            }
            invSer.eliminarporId(id);
            return ResponseEntity.ok().build();
        }

        
}