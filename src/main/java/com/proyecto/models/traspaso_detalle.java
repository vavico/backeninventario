package com.proyecto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "traspaso_detalle")
public class traspaso_detalle {
	@Id
    @Column(name="tdet_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "TDetalleTable")
    private int tdet_id;
	
	@ManyToOne
    @JoinColumn(name = "tdet_cab_id")
    private Traspaso_cabecera tdet_cab_id;

	@ManyToOne
	@JoinColumn(name = "tdet_inv_id")
    private inventario tdet_inv_id;
	
	@Column(name="tdet_cedula")
    private String tdet_cedula;
	
	public int getTdet_id() {
		return tdet_id;
	}
	public void setTdet_id(int tdet_id) {
		this.tdet_id = tdet_id;
	}
	public Traspaso_cabecera getTdet_cab_id() {
		return tdet_cab_id;
	}
	public void setTdet_cab_id(Traspaso_cabecera tdet_cab_id) {
		this.tdet_cab_id = tdet_cab_id;
	}
	public inventario getTdet_inv_id() {
		return tdet_inv_id;
	}
	public void setTdet_inv_id(inventario tdet_inv_id) {
		this.tdet_inv_id = tdet_inv_id;
	}
	public String getTdet_cedula() {
		return tdet_cedula;
	}
	public void setTdet_cedula(String tdet_cedula) {
		this.tdet_cedula = tdet_cedula;
	}
}
