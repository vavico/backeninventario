package com.proyecto.models;

// Clase para crear la tabla articulo

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "articulo")

public class articulo {

    @Id
    @Column(name="art_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "ArticuloTable")
    private int art_id;
    @Column(name = "art_codigo", unique=true)
    private String art_codigo;
    @Column(name = "art_cod_anterior")
    private String art_cod_anterior;
    @Column(name = "art_numero_acta")
    private String art_numero_acta;
    @Column(name = "art_bld")
    private String art_bld;
    @Column(name = "art_bien")
    private String art_bien;
    @Column(name = "art_serie")
    private String art_serie;
    @Column(name = "art_modelo")
    private String art_modelo;
    @Column(name = "art_marca")
    private String art_marca;
    @Column(name = "art_color")
    private String art_color;
    @Column(name = "art_material")
    private String art_material;
    @Column(name = "art_dimensiones")
    private String art_dimensiones;
    @Column(name = "art_descripcion")
    private String art_descripcion;
    @Column(name = "art_aula")
    private String art_aula;
    @Column(name = "art_cuenta_contable")
    private String art_cuenta_contable;
    @Column(name = "art_fecha_ing")
    private String art_fecha_ing;
    @Column(name = "art_fecha_depre")
    private String art_fecha_depre;
    @Column(name = "art_valor_contable")
    private String art_valor_contable;
    @Column(name = "art_observacion")
    private String art_observacion;
    @Column(name = "art_conciliacion")
    private String art_conciliacion;
    @Column(name = "art_estado")
    private String art_estado;
    @Column(name = "art_categoria")
    private String art_categoria;
    @Column(name = "art_empresa")
    private String art_empresa;
    @Column(name = "art_asignado")
    private String art_asignado;
	
    public int getArt_id() {
		return art_id;
	}
	public void setArt_id(int art_id) {
		this.art_id = art_id;
	}
	public String getArt_codigo() {
		return art_codigo;
	}
	public void setArt_codigo(String art_codigo) {
		this.art_codigo = art_codigo;
	}
	public String getArt_cod_anterior() {
		return art_cod_anterior;
	}
	public void setArt_cod_anterior(String art_cod_anterior) {
		this.art_cod_anterior = art_cod_anterior;
	}
	public String getArt_numero_acta() {
		return art_numero_acta;
	}
	public void setArt_numero_acta(String art_numero_acta) {
		this.art_numero_acta = art_numero_acta;
	}
	public String getArt_bld() {
		return art_bld;
	}
	public void setArt_bld(String art_bld) {
		this.art_bld = art_bld;
	}
	public String getArt_bien() {
		return art_bien;
	}
	public void setArt_bien(String art_bien) {
		this.art_bien = art_bien;
	}
	public String getArt_serie() {
		return art_serie;
	}
	public void setArt_serie(String art_serie) {
		this.art_serie = art_serie;
	}
	public String getArt_modelo() {
		return art_modelo;
	}
	public void setArt_modelo(String art_modelo) {
		this.art_modelo = art_modelo;
	}
	public String getArt_marca() {
		return art_marca;
	}
	public void setArt_marca(String art_marca) {
		this.art_marca = art_marca;
	}
	public String getArt_color() {
		return art_color;
	}
	public void setArt_color(String art_color) {
		this.art_color = art_color;
	}
	public String getArt_material() {
		return art_material;
	}
	public void setArt_material(String art_material) {
		this.art_material = art_material;
	}
	public String getArt_dimensiones() {
		return art_dimensiones;
	}
	public void setArt_dimensiones(String art_dimensiones) {
		this.art_dimensiones = art_dimensiones;
	}
	public String getArt_descripcion() {
		return art_descripcion;
	}
	public void setArt_descripcion(String art_descripcion) {
		this.art_descripcion = art_descripcion;
	}
	public String getArt_aula() {
		return art_aula;
	}
	public void setArt_aula(String art_aula) {
		this.art_aula = art_aula;
	}
	public String getArt_cuenta_contable() {
		return art_cuenta_contable;
	}
	public void setArt_cuenta_contable(String art_cuenta_contable) {
		this.art_cuenta_contable = art_cuenta_contable;
	}
	public String getArt_fecha_ing() {
		return art_fecha_ing;
	}
	public void setArt_fecha_ing(String art_fecha_ing) {
		this.art_fecha_ing = art_fecha_ing;
	}
	public String getArt_fecha_depre() {
		return art_fecha_depre;
	}
	public void setArt_fecha_depre(String art_fecha_depre) {
		this.art_fecha_depre = art_fecha_depre;
	}
	public String getArt_valor_contable() {
		return art_valor_contable;
	}
	public void setArt_valor_contable(String art_valor_contable) {
		this.art_valor_contable = art_valor_contable;
	}
	public String getArt_observacion() {
		return art_observacion;
	}
	public void setArt_observacion(String art_observacion){
		this.art_observacion = art_observacion;
	}
	public String getArt_conciliacion() {
		return art_conciliacion;
	}
	public void setArt_conciliacion(String art_conciliacion) {
		this.art_conciliacion = art_conciliacion;
	}
	public String getArt_estado() {
		return art_estado;
	}
	public void setArt_estado(String art_estado) {
		this.art_estado = art_estado;
	}
	public String getArt_categoria() {
		return art_categoria;
	}
	public void setArt_categoria(String art_categoria) {
		this.art_categoria = art_categoria;
	}
	public String getArt_empresa() {
		return art_empresa;
	}
	public void setArt_empresa(String art_empresa) {
		this.art_empresa = art_empresa;
	}
	public String getArt_asignado() {
		return art_asignado;
	}
	public void setArt_asignado(String art_asignado) {
		this.art_asignado = art_asignado;
	}
}