package com.proyecto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TCabecera")
public class Traspaso_cabecera {
	@Id
    @Column(name="tcab_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "TCabeceraTable")
    private int tcab_id;
	@Column(name = "tcab_fecha")
    private String tcab_fecha;
	
	public int getTcab_id() {
		return tcab_id;
	}
	public void setTcab_id(int tcab_id) {
		this.tcab_id = tcab_id;
	}
	public String getTcab_fecha() {
		return tcab_fecha;
	}
	public void setTcab_fecha(String tcab_fecha) {
		this.tcab_fecha = tcab_fecha;
	}
}
