package com.proyecto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.models.usuario;;

@Repository
public interface IUsuarioRepo extends JpaRepository<usuario, Integer> {
	// Busca por cedula
    @Query(value = "select * from usuario where usu_cedula=?",nativeQuery = true)
    usuario findbycedula(String usu_cedula);
    
    // Busca por usuario
    @Query(value = "select * from usuario where usu_usuario=?",nativeQuery = true)
    usuario findbyusuario(String usu_usuario);

    // Busca por rol
    @Query(value = "select * from usuario where usu_rol=?",nativeQuery = true)
    List<usuario> findbyRol(String usu_rol);
    
    // Busca por id para editar
    @Query(value = "select * from usuario where usu_id=?",nativeQuery = true)
    usuario findbyId(int usu_id);

    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from usuario where usu_id=?", nativeQuery =true)
    int deleteById(int usu_id);

}
