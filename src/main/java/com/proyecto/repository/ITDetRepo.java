package com.proyecto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.models.traspaso_detalle;

@Repository
public interface ITDetRepo extends JpaRepository<traspaso_detalle, Integer>{
	// Busca por cedula
    @Query(value = "select * from TDetalle where tdet_cedula=?",nativeQuery = true)
    List<traspaso_detalle> findbycedula(String tdet_cedula);
    
    // Busca por id para editar
    @Query(value = "select * from TDetalle where tdet_id=?",nativeQuery = true)
    traspaso_detalle findbyId(int tcab_id);

    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from TDetalle where tdet_id=?", nativeQuery =true)
    int deleteById(int tcab_id);
}
