package com.proyecto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.models.Traspaso_cabecera;

@Repository
public interface ITCabRepo extends JpaRepository<Traspaso_cabecera, Integer>{
	// Busca por nombre
    @Query(value = "select * from TCabecera where tcab_fecha=?",nativeQuery = true)
    List<Traspaso_cabecera> findbyfecha(String tcab_fecha);
    
    // Busca por id para editar
    @Query(value = "select * from TCabecera where tcab_id=?",nativeQuery = true)
    Traspaso_cabecera findbyId(int tcab_id);

    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from TCabecera where tcab_id=?", nativeQuery =true)
    int deleteById(int tcab_id);
}
