package com.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.proyecto.models.inventario;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

@Repository

public interface IInventarioRepo extends JpaRepository<inventario, Integer>{
    
        // Buscar por codigo de articulo
        @Query(value = "select * from inventario where inv_art_codigo=?",nativeQuery = true)
        List<inventario> findbybuscar(String buscar);
        
        // Buscar por cedula
        @Query(value = "select * from inventario where inv_doc_cedula=?",nativeQuery = true)
        List<inventario> findbyCedula(String buscar);
        
        // Buscar para editar por id, retorna un solo valor
        @Query(value = "select * from inventario where inv_id=?",nativeQuery = true)
        inventario findbyId(int inv_id);
        
        // Eliminar por id
        @Transactional
        @Modifying
        @Query(value="delete from inventario where inv_id=?", nativeQuery =true)
        int deleteById(int inv_id);
        
}
