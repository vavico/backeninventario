package com.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.models.categorias;

@Repository
public interface ICategoriaRepo extends JpaRepository<categorias, Integer> {
	// Busca por nombre
    @Query(value = "select * from categorias where cat_nombre=?",nativeQuery = true)
    categorias findbynombre(String cat_nombre);
    
    // Busca por id para editar
    @Query(value = "select * from categorias where cat_id=?",nativeQuery = true)
    categorias findbyId(int cat_id);

    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from categorias where cat_id=?", nativeQuery =true)
    int deleteById(int cat_id);
}
