package com.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.proyecto.models.articulo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

@Repository

public interface IArticuloRepo extends JpaRepository<articulo, Integer>{
	
	@Query(value = "select a from articulo a order by a.art_aula asc")
    Page<articulo> ordenarPorCodigo(Pageable page);
	
    // Busca por codigo
	@Query(value = "select a from articulo a where a.art_codigo like %:text% or a.art_bien like %:text% or a.art_empresa like %:text% or a.art_aula like %:text%")
    Page<articulo> filtrar(@Param("text") String text, Pageable page);
	
    // ver no asignados
    @Query(value = "select a from articulo a where a.art_asignado like %:text%")
    Page<articulo> filtrar_asignado(@Param("text") String text, Pageable page);
    
    // Busca por id para editar
    @Query(value = "select * from articulo where art_id=?",nativeQuery = true)
    articulo findbyId(int art_id);

 // Busca por codigo
    @Query(value = "select * from articulo where art_codigo=?",nativeQuery = true)
    articulo findbyCodigo(String art_codigo);
    
    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from articulo where art_id=?", nativeQuery =true)
    int deleteById(int art_id);
}
