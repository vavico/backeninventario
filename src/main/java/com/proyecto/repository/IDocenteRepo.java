package com.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.proyecto.models.docente;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository

public interface IDocenteRepo extends JpaRepository<docente, Integer>{
    // Buscar por parametros, retorna una lista
    @Query(value = "select * from docente where doc_cedula=? or doc_nombre=? or doc_apellido=?",nativeQuery = true)
    List<docente> findbyparametro(String parametro,String parametro2,String parametro3);

    // Buscar para editar por id, retorna un solo valor
    @Query(value = "select * from docente where doc_id=?",nativeQuery = true)
    docente findbyId(int doc_id);

    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from docente where doc_id=?", nativeQuery =true)
    int deleteById(int doc_id);

}