package com.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.models.categorias;
import com.proyecto.repository.ICategoriaRepo;

@Service
public class CategoriaService {
	@Autowired
    private ICategoriaRepo catRepo;
    //guardar
    public categorias crearCategoria(categorias cat) {
        return catRepo.save(cat);
    }

    //listar
    public List<categorias> listar(){
        return catRepo.findAll();
    }

    //buscar por codigo
    public categorias buscarpornombre(String nombre) {
        return catRepo.findbynombre(nombre);
    }
    
    //buscar por id
    public categorias buscarporid(int id) {
        return catRepo.findbyId(id);
    }
    
    //eliminar
    public void eliminarporId (int id){
        catRepo.deleteById(id);
    }
}
