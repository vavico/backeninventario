package com.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.models.Traspaso_cabecera;
import com.proyecto.repository.ITCabRepo;

@Service
public class CabeceraService {
	@Autowired
    private ITCabRepo cabRepo;
    //guardar
    public Traspaso_cabecera crearCabecera(Traspaso_cabecera cab) {
        return cabRepo.save(cab);
    }

    //listar
    public List<Traspaso_cabecera> listar(){
        return cabRepo.findAll();
    }

    //buscar por nombre
    public List<Traspaso_cabecera> buscarporfecha(String fecha) {
        return cabRepo.findbyfecha(fecha);
    }
    
    //buscar por id
    public Traspaso_cabecera buscarporid(int id) {
        return cabRepo.findbyId(id);
    }
    
    //eliminar
    public void eliminarporId (int id){
    	cabRepo.deleteById(id);
    }
}
