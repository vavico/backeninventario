package com.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.models.edificio;
import com.proyecto.repository.IEdificioRepo;

@Service
public class EdificioService {
	@Autowired
    private IEdificioRepo edifRepo;
    //guardar
    public edificio crearEdificio(edificio edif) {
        return edifRepo.save(edif);
    }

    //listar
    public List<edificio> listar(){
        return edifRepo.findAll();
    }

    //buscar por nombre
    public edificio buscarpornombre(String nombre) {
        return edifRepo.findbynombre(nombre);
    }
    
    //buscar por id
    public edificio buscarporid(int id) {
        return edifRepo.findbyId(id);
    }
    
    //eliminar
    public void eliminarporId (int id){
    	edifRepo.deleteById(id);
    }
}

