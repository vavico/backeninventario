package com.proyecto.service;



//Clase de microservicio

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import com.proyecto.models.articulo;
import com.proyecto.repository.IArticuloRepo;

import org.springframework.data.domain.Pageable;

@Service

public class ArticuloService {
	
    @Autowired
    private IArticuloRepo artRepo;
    //guardar
    public articulo crearArticulo(articulo art) {
        return artRepo.save(art);
    }

    //listar
    public Page<articulo> listar(Pageable page){
        return artRepo.ordenarPorCodigo(page);
    }

    public articulo buscarporcodigo(String codigo) {
    	return artRepo.findbyCodigo(codigo);
    }
    
    //buscar por código, bien, empresa, ubicación
    public Page<articulo> filtrar(String codigo,Pageable page) {
        return artRepo.filtrar(codigo, page);
    }
    
    //buscar no asignados
    public Page<articulo> verAsignado(String asignado,Pageable page) {
        return artRepo.filtrar_asignado(asignado,page);
    }
    
    public articulo buscarporid(int id) {
    	return artRepo.findbyId(id);
    }

    //eliminar
    public void eliminarporId (int id){
        artRepo.deleteById(id);
    }
}
