package com.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.models.traspaso_detalle;
import com.proyecto.repository.ITDetRepo;

@Service
public class DetalleService {
	@Autowired
    private ITDetRepo detRepo;
    //guardar
    public traspaso_detalle crearEdificio(traspaso_detalle det) {
        return detRepo.save(det);
    }

    //listar
    public List<traspaso_detalle> listar(){
        return detRepo.findAll();
    }

    //buscar por nombre
    public List<traspaso_detalle> buscarporcedula(String cedula) {
        return detRepo.findbycedula(cedula);
    }
    
    //buscar por id
    public traspaso_detalle buscarporid(int id) {
        return detRepo.findbyId(id);
    }
    
    //eliminar
    public void eliminarporId (int id){
    	detRepo.deleteById(id);
    }
}
