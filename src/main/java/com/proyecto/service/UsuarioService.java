package com.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.models.usuario;
import com.proyecto.repository.IUsuarioRepo;

@Service
public class UsuarioService {
	@Autowired
    private IUsuarioRepo usuRepo;
    //guardar
    public usuario crearUsuario(usuario usu) {
        return usuRepo.save(usu);
    }

    //listar
    public List<usuario> listar(){
        return usuRepo.findAll();
    }

    //buscar por cedula
    public usuario buscarporcedula(String cedula) {
        return usuRepo.findbycedula(cedula);
    }
    
    //buscar por usuario
    public usuario buscarporusuario(String usuario) {
        return usuRepo.findbyusuario(usuario);
    }

    //buscar por rol
    public List<usuario> buscarporRol(String rol) {
        return usuRepo.findbyRol(rol);
    }
    
    //buscar por id
    public usuario buscarporid(int id) {
        return usuRepo.findbyId(id);
    }
    
    //eliminar
    public void eliminarporId (int id){
        usuRepo.deleteById(id);
    }
}
